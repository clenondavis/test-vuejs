(function(){
	'use strict';

	var projects = [
		{
			id: 1,
			name: 'Project 1',
			description: 'Testing project 1',
			progress: 50,
			active: true
		},
		{
			id: 2,
			name: 'Project 2',
			description: 'Testing project 2',
			progress: 100,
			active: false
		}
	];

	Vue.component('project-list', {
		props: {
			list: { type: Array, required: true }
		},
		template: '#project-tmpl',
	});

	var vm = new Vue({
		el: '#app',
		data: {
			projects: projects
		},
		methods: {
			setProjects: setProjects
		}
	});

	function setProjects(category) {
		projects = [];

		switch (category) {
			case 'ui':				
				projects = [
					{
						id: 1,
						name: 'Project 1',
						description: 'Testing project 1',
						progress: 50,
						active: true
					},
					{
						id: 2,
						name: 'Project 2',
						description: 'Testing project 2',
						progress: 100,
						active: false
					}
				];
				break;
			case 'web':				
				projects = [
					{
						id: 1,
						name: 'Web 1',
						description: 'Testing Web 1',
						progress: 50,
						active: true
					},
					{
						id: 2,
						name: 'Web 2',
						description: 'Testing Web 2',
						progress: 100,
						active: false
					}
				];
				break;
		}

		this.projects = projects;
	}

})();